# Api-accident

Api-accident restitue sous forme d'API REST les données des accidents corporels de la circulation routière en France
provenant d'OpenData (data.gouv.fr)

## Environnement de développement

### Pré requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose

Vérification des prés-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
docker-compose up -d
symfony serve -d
```