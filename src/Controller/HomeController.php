<?php

namespace App\Controller;

use App\Entity\CountTotal;
use App\Entity\InfosDate;
use App\Entity\Topcity;
use App\Entity\TopPlageHoraire;
use App\Entity\TopRegion;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        $topCity = $this->getDoctrine()
            ->getRepository(Topcity::class)
            ->findAll();

        $topRegion = $this->getDoctrine()
            ->getRepository(TopRegion::class)
            ->findAll();

        $topPlage = $this->getDoctrine()
            ->getRepository(TopPlageHoraire::class)
            ->findAll();

        $dateInfos = $this->getDoctrine()
            ->getRepository(InfosDate::class)
            ->find(1);

        $total = $this->getDoctrine()
            ->getRepository(CountTotal::class)
            ->find(1);

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'topCity'         => $topCity,
            'topPlage'        => $topPlage,
            'topRegion'       => $topRegion,
            'dateInfos'       => $dateInfos,
            'total'           => $total,
        ]);
    }
}
