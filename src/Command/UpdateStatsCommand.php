<?php

namespace App\Command;

use App\Entity\CountTotal;
use App\Entity\InfosDate;
use App\Entity\Topcity;
use App\Entity\TopPlageHoraire;
use App\Entity\TopRegion;
use App\Repository\AccidentCaracteristiqueRepository;
use App\Repository\AccidentCorporelRepository;
use App\Repository\CountTotalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateStatsCommand extends Command
{
    protected static $defaultName = 'app:update-stats';

    private AccidentCaracteristiqueRepository $aRepository;

    private AccidentCorporelRepository $sRepository;

    private CountTotalRepository $cRepository;

    private EntityManagerInterface $entityManager;

    private string $projectDir;


    public function __construct(
        string $projectDir,
        EntityManagerInterface $entityManager,
        AccidentCorporelRepository $sRepository,
        AccidentCaracteristiqueRepository $aRepository,
        CountTotalRepository $cRepository
    ) {
        $this->projectDir = $projectDir;
        $this->entityManager = $entityManager;
        $this->sRepository = $sRepository;
        $this->aRepository = $aRepository;
        $this->cRepository = $cRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Update Stats')
            ->addArgument('markup', InputArgument::OPTIONAL, 'Percentage markup', 20)
            ->addArgument(
                'process_date',
                InputArgument::OPTIONAL,
                'Date of the process',
                date_create()->format('Y-m-d')
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->updateStatsData();
        return Command::SUCCESS;
    }

    private function updateStatsData(): void
    {
        $num = $this->sRepository ->createQueryBuilder('u')
            ->select('count(u.num_accident)')
            ->getQuery()
            ->getSingleScalarResult();

        $topCity = new CountTotal();
        $topCity->setNb($num);
        $this->entityManager->persist($topCity);
        $this->entityManager->flush();

        $max_date = $this->sRepository
            ->createQueryBuilder('u')
            ->select('max(u.date)')
            ->getQuery()
            ->getSingleScalarResult();

        $min_date = $this
            ->sRepository
            ->createQueryBuilder('u')
            ->select('min(u.date)')
            ->getQuery()
            ->getSingleScalarResult();

        $topDate = new InfosDate();
        $topDate->setDateMax(new \DateTime($max_date));
        $topDate->setDateMin(new \DateTime($min_date));
        $this->entityManager->persist($topDate);
        $this->entityManager->flush();

        $top_ville = $this->sRepository ->createQueryBuilder('u')
            ->select('u.ville,u.code_postal,count(u.num_accident) as nb')
            ->where("u.ville != ''")
            ->groupBy("u.ville,u.code_postal")
            ->orderBy("count(u.num_accident)", "desc")
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();

        foreach ($top_ville as $ville) {
            $city = new Topcity();
            $city->setVille($ville['ville']);
            $city->setNombre($ville['nb']);
            $city->setCityCode($ville['code_postal']);
            $this->entityManager->persist($city);
            $this->entityManager->flush();
        }

        $top_region = $this->sRepository ->createQueryBuilder('u')
            ->select('u.nom_region as nom_region,count(u.num_accident) as nb')
            ->where("u.nom_region != ''")
            ->groupBy("u.nom_region")
            ->orderBy("count(u.num_accident)", "desc")
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();

        foreach ($top_region as $region) {
            $reg = new TopRegion();
            $reg->setRegion($region['nom_region']);
            $reg->setNombre($region['nb']);
            $this->entityManager->persist($reg);
            $this->entityManager->flush();
        }

        $top_plage_horaire = $this->sRepository ->createQueryBuilder('u')
            ->select('u.plage_horaire,count(u.num_accident) as nb')
            ->where("u.plage_horaire != ''")
            ->groupBy("u.plage_horaire")
            ->orderBy("count(u.num_accident)", "desc")
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();

        foreach ($top_plage_horaire as $plage) {
            $topPlage = new TopPlageHoraire();
            $topPlage->setPlageHoraire($plage['plage_horaire']);
            $topPlage->setNb($plage['nb']);
            $this->entityManager->persist($topPlage);
            $this->entityManager->flush();
        }
    }
}
