<?php

namespace App\Command;

use App\Entity\AccidentCaracteristique;
use App\Entity\AccidentCorporel;
use App\Repository\AccidentCaracteristiqueRepository;
use App\Repository\AccidentCorporelRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class UpdateAccidentCommand extends Command
{
    protected static $defaultName = 'app:update-accident-data';

    private AccidentCaracteristiqueRepository $aRepository;

    private AccidentCorporelRepository $sRepository;

    private EntityManagerInterface $entityManager;

    private string $projectDir;

    private CacheInterface $cache;

    public function __construct(
        string $projectDir,
        EntityManagerInterface $entityManager,
        AccidentCorporelRepository $sRepository,
        AccidentCaracteristiqueRepository $aRepository,
        CacheInterface $cache
    ) {
        $this->projectDir = $projectDir;
        $this->entityManager = $entityManager;
        $this->sRepository = $sRepository;
        $this->aRepository = $aRepository;
        $this->cache = $cache;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Update Accident Corporel')
            ->addArgument('markup', InputArgument::OPTIONAL, 'Percentage markup', 20)
            ->addArgument(
                'process_date',
                InputArgument::OPTIONAL,
                'Date of the process',
                date_create()->format('Y-m-d')
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->createLigneAccident();
        return Command::SUCCESS;
    }

    private function getCsvRowsAsArrays(): array
    {
        $inputFile = $this->projectDir . '/public/accident-data-files/accidents.csv';
        $decoder = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        return $decoder->decode(file_get_contents($inputFile), 'csv', array(CsvEncoder::DELIMITER_KEY => ';'));
    }

    private function createLigneAccident(): void
    {

        // CSV
        $csv = $this->cache->get('csvAccidentCorporel', function (ItemInterface $item) {
            $item->expiresAfter(\DateInterval::createFromDateString('1 year'));
            return  $this->getCsvRowsAsArrays();
        });
        $ligneCree = 0;
        $compteur = 0;
        $acc = [];
        $accCar = [];

        foreach ($csv as $row) {
            $date = new DateTime($row['date']);
            $row['voie'] = ($row['voie'] == '' || !is_numeric($row['voie'])) ? null : $row['voie'];
            $row['latitude'] = ($row['latitude'] == '') ? null : $row['latitude'];
            $row['longitude'] = ($row['longitude'] == '') ? null : $row['longitude'];
            $row['geo_score'] = ($row['geo_score'] == '' || is_string($row['geo_score'])) ? null : $row['geo_score'];
            $row['PR_display'] = (!isset($row['PR_display']) || $row['PR_display'] == '') ? 0 : $row['PR_display'];
            $row['geo_source_display'] = ($row['geo_source_display'] == '') ? null : $row['geo_source_display'];
            $row['num_route_com_id'] = (strlen($row['num_route_com_id'])  > 250) ?
            substr($row['num_route_com_id'], 0, 250) : $row['num_route_com_id'];
            $row['distance'] = ($row['distance'] == '') ? null : floatval($row['distance']);
            $row['lartpc'] = ($row['lartpc'] == '') ? null : floatval($row['lartpc']);
            $row['nbv'] = ($row['nbv'] == '') ? null : floatval($row['nbv']);
            $row['larrout'] = ($row['larrout'] == '') ? null : floatval($row['larrout']);
            $row['pr1'] = ($row['pr1'] == '' || !is_numeric($row['pr1'])) ? null : $row['pr1'];
            $row['infra'] = ($row['infra'] == '') ? null : $row['infra'];
            $row['voiture_nb'] = (!is_numeric($row['voiture_nb'])) ? 0 : $row['voiture_nb'];
            $row['velo_nb'] = (!is_numeric($row['velo_nb'])) ? 0 : $row['velo_nb'];
            $row['pietons_nb'] = (!is_numeric($row['pietons_nb'])) ? 0 : $row['pietons_nb'];
            $row['vehiculeautre_nb'] = (!is_numeric($row['vehiculeautre_nb'])) ? 0 : $row['vehiculeautre_nb'];
            $row['poidslourd_nb'] = (!is_numeric($row['poidslourd_nb'])) ? 0 : $row['poidslourd_nb'];
            $row['tue_nb'] = (!is_numeric($row['tue_nb'])) ? 0 : $row['tue_nb'];

            // Accident base
            $acc[$compteur]['num_accident'] = $row['Num_Acc'] ;
            $acc[$compteur]['mois'] = $row['mois'] ;
            $acc[$compteur]['date'] = $date->format('Y-m-d H:i:s');
            $acc[$compteur]['adresse'] = $row['adr'] ;
            $acc[$compteur]['departement'] = $row['dep'] ;
            $acc[$compteur]['date_formated'] = $row['date_formated'] ;
            $acc[$compteur]['heures_minutes'] = $row['heures_minutes'] ;
            $acc[$compteur]['annee'] = $row['ANNEE'] ;
            $acc[$compteur]['semaine'] = $row['SEMAINE'] ;
            $acc[$compteur]['libelle_jour'] = $row['LIBELLE_JOUR'] ;
            $acc[$compteur]['heure'] = $row['HEURE'] ;
            $acc[$compteur]['plage_horaire'] = $row['LIBELLE_PLAGE_HORAIRE'] ;
            $acc[$compteur]['code_postal'] = $row['original_city_code'] ;
            $acc[$compteur]['ville'] = $row['original_name'] ;
            $acc[$compteur]['latitude'] = $row['latitude'] ;
            $acc[$compteur]['geo_type'] = $row['geo_type'] ;
            $acc[$compteur]['longitude'] = $row['longitude'] ;
            $acc[$compteur]['geo_score'] = $row['geo_score'] ;
            $acc[$compteur]['geo_source'] = $row['geo_source_display'] ;
            $acc[$compteur]['num_route_or_id'] = $row['num_route_or_id'] ;
            $acc[$compteur]['num_route_com_id'] = $row['num_route_com_id'] ;
            $acc[$compteur]['voie'] = $row['voie'] ;
            $acc[$compteur]['indice_numerique_route'] = $row['v1'] ;
            $acc[$compteur]['indice_alphabetique_route'] = $row['v2'] ;
            $acc[$compteur]['nom_region'] = $row['NOM_REG'] ;

            // Accident caractéristique
            $accCAr[$compteur]['num_accident'] = $row['Num_Acc'] ;
            $accCAr[$compteur]['luminosite'] = $row['lum'] ;
            $accCAr[$compteur]['agglomeration'] = $row['agg'] ;
            $accCAr[$compteur]['intersection'] = $row['int'] ;
            $accCAr[$compteur]['meteo'] = $row['atm'] ;
            $accCAr[$compteur]['type_collision'] = $row['col'] ;
            $accCAr[$compteur]['distance'] = $row['distance'];
            $accCAr[$compteur]['categorie_route'] = $row['catr'] ;
            $accCAr[$compteur]['regime_circulation'] = $row['circ'] ;
            $accCAr[$compteur]['nombre_voies_circulation'] = $row['nbv'] ;
            $accCAr[$compteur]['numero_pr_rapprochement'] = $row['pr'] ;
            $accCAr[$compteur]['distance_pr_rapprochement'] = $row['pr1'] ;
            $accCAr[$compteur]['voie_reservee'] = $row['infra'] ;
            $accCAr[$compteur]['profil_route'] = $row['prof'] ;
            $accCAr[$compteur]['plan_route'] = $row['plan'] ;
            $accCAr[$compteur]['largeur_terre_plein_central'] = $row['lartpc'] ;
            $accCAr[$compteur]['largeur_chaussee'] = $row['larrout'] ;
            $accCAr[$compteur]['etat_route'] = $row['surf'] ;
            $accCAr[$compteur]['infrastructure_route'] = $row['infra'] ;
            $accCAr[$compteur]['situation_accident'] = $row['situ'] ;
            $accCAr[$compteur]['environnement_special'] = $row['env1'] ;
            $accCAr[$compteur]['nombre_voiture'] = $row['voiture_nb'] ;
            $accCAr[$compteur]['presence_voiture'] = $row['has_voiture'] ;
            $accCAr[$compteur]['nombre_velo'] = $row['velo_nb'] ;
            $accCAr[$compteur]['presence_velo'] = $row['has_velo'] ;
            ;
            $accCAr[$compteur]['nombre_poids_lourd'] = $row['poidslourd_nb'] ;
            $accCAr[$compteur]['presence_poid_lourd'] = $row['has_poidslourd'] ;
            $accCAr[$compteur]['nombre_autre_vehicule'] = $row['vehiculeautre_nb'] ;
            $accCAr[$compteur]['presence_autre_vehicule'] = $row['has_vehiculeautre'] ;
            $accCAr[$compteur]['nombre_pieton'] = $row['pietons_nb'] ;
            $accCAr[$compteur]['presence_pieton'] = $row['has_pietons'] ;
            $accCAr[$compteur]['nombre_indemne'] = $row['indemne_nb'] ;
            $accCAr[$compteur]['nombre_blesse_leger'] = $row['blesseleger_nb'] ;
            $accCAr[$compteur]['nombre_hospitalise'] = $row['hospitalise_nb'] ;
            $accCAr[$compteur]['nombre_tue'] = $row['tue_nb'] ;
            $accCAr[$compteur]['gravite_accident'] = $row['gravite_accident'] ;
            $compteur++;
        }

        $this->sRepository->insertionEnMasse($acc);
        $this->aRepository->insertionEnMasse($accCAr);
    }
}
