<?php

namespace App\Repository;

use App\Entity\TopRegion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TopRegion|null find($id, $lockMode = null, $lockVersion = null)
 * @method TopRegion|null findOneBy(array $criteria, array $orderBy = null)
 * @method TopRegion[]    findAll()
 * @method TopRegion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TopRegionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TopRegion::class);
    }

    // /**
    //  * @return TopRegion[] Returns an array of TopRegion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TopRegion
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
