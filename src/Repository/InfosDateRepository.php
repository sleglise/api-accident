<?php

namespace App\Repository;

use App\Entity\InfosDate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InfosDate|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfosDate|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfosDate[]    findAll()
 * @method InfosDate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfosDateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfosDate::class);
    }

    // /**
    //  * @return InfosDate[] Returns an array of InfosDate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InfosDate
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
