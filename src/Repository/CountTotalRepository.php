<?php

namespace App\Repository;

use App\Entity\CountTotal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CountTotal|null find($id, $lockMode = null, $lockVersion = null)
 * @method CountTotal|null findOneBy(array $criteria, array $orderBy = null)
 * @method CountTotal[]    findAll()
 * @method CountTotal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CountTotalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CountTotal::class);
    }

    // /**
    //  * @return CountTotal[] Returns an array of CountTotal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CountTotal
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
