<?php

namespace App\Repository;

use App\Entity\AccidentCorporel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccidentCorporel|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccidentCorporel|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccidentCorporel[]    findAll()
 * @method AccidentCorporel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccidentCorporelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccidentCorporel::class);
    }

    public function insertionEnMasse(array $tab): void
    {
        $conn = $this->getEntityManager()->getConnection();

        if (!empty($tab)) {
            if (isset($tab[0])) {
                $champs = array_keys($tab[0]);
            } else {
                return;
            }

            $batch = array_chunk($tab, 3000);
            foreach ($batch as $ba) {
                foreach ($ba as $k => $v) {
                    $key[] = $k;
                    foreach ($v as $s => $x) {
                        $parametres[] = $x;
                    }
                    $ligne_values[] = '(' . implode(', ', array_fill(0, count($v), '?')) . ')';
                }

                $sql = 'REPLACE INTO accident_corporel (' . implode(',', $champs) . ') VALUES ' .
                implode(",", $ligne_values) . '  ;';

                $stmt = $conn->prepare($sql);
                $stmt->execute($parametres);
            }
        }
        return;
    }
}
