<?php

namespace App\Repository;

use App\Entity\Topcity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Topcity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Topcity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Topcity[]    findAll()
 * @method Topcity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TopcityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Topcity::class);
    }

    // /**
    //  * @return Topcity[] Returns an array of Topcity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Topcity
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
