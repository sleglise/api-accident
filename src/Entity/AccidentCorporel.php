<?php

namespace App\Entity;

use App\Repository\AccidentCorporelRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass=AccidentCorporelRepository::class)
 * @ApiResource(
 *      normalizationContext={"groups"={"read:accident"}},
 *      collectionOperations={"get"},
 *      itemOperations={"get"}
 *
 * )
 * @ApiFilter(SearchFilter::class,properties={"ville":"exact"})
 */
class AccidentCorporel
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="bigint")
     * @Groups({"read:accident"})
     */
    private $num_accident;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read:accident"})
     */
    private $mois;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:accident"})
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     * @Groups({"read:accident"})
     */
    private $departement;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"read:accident"})
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Groups({"read:accident"})
     */
    private $date_formated;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @Groups({"read:accident"})
     */
    private $heures_minutes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read:accident"})
     */
    private $annee;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read:accident"})
     */
    private $semaine;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Groups({"read:accident"})
     */
    private $libelle_jour;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read:accident"})
     */
    private $heure;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Groups({"read:accident"})
     */
    private $plage_horaire;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     * @Groups({"read:accident"})
     */
    private $code_postal;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     * @Groups({"read:accident"})
     */
    private $ville;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"read:accident"})
     */
    private $latitude;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"read:accident"})
     */
    private $geo_type;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"read:accident"})
     */
    private $longitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $geo_score;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $geo_source;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:accident"})
     */
    private $num_route_or_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:accident"})
     */
    private $num_route_com_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read:accident"})
     */
    private $voie;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Groups({"read:accident"})
     */
    private $indice_numerique_route;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"read:accident"})
     */
    private $indice_alphabetique_route;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"read:accident"})
     */
    private $nom_region;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $geo_source_display;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $pr_display;

    /**
     * @ORM\OneToMany(targetEntity="AccidentCaracteristique", mappedBy="num_accident")
     * @ApiSubResource
     */
    private $accidentcaracteristiques;

    public function getNumAccident(): ?int
    {
        return $this->num_accident;
    }

    public function setNumAccident(?int $num_accident): self
    {
        $this->num_accident = $num_accident;

        return $this;
    }


    public function getMois(): ?int
    {
        return $this->mois;
    }

    public function setMois(?int $mois): self
    {
        $this->mois = $mois;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(?string $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDateFormated(): ?string
    {
        return $this->date_formated;
    }

    public function setDateFormated(?string $date_formated): self
    {
        $this->date_formated = $date_formated;

        return $this;
    }

    public function getHeuresMinutes(): ?string
    {
        return $this->heures_minutes;
    }

    public function setHeuresMinutes(?string $heures_minutes): self
    {
        $this->heures_minutes = $heures_minutes;

        return $this;
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getSemaine(): ?int
    {
        return $this->semaine;
    }

    public function setSemaine(?int $semaine): self
    {
        $this->semaine = $semaine;

        return $this;
    }

    public function getLibelleJour(): ?string
    {
        return $this->libelle_jour;
    }

    public function setLibelleJour(?string $libelle_jour): self
    {
        $this->libelle_jour = $libelle_jour;

        return $this;
    }

    public function getHeure(): ?int
    {
        return $this->heure;
    }

    public function setHeure(?int $heure): self
    {
        $this->heure = $heure;

        return $this;
    }

    public function getPlageHoraire(): ?string
    {
        return $this->plage_horaire;
    }

    public function setPlageHoraire(?string $plage_horaire): self
    {
        $this->plage_horaire = $plage_horaire;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->code_postal;
    }

    public function setCodePostal(?string $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getGeoType(): ?string
    {
        return $this->geo_type;
    }

    public function setGeoType(?string $geo_type): self
    {
        $this->geo_type = $geo_type;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getGeoScore(): ?float
    {
        return $this->geo_score;
    }

    public function setGeoScore(?float $geo_score): self
    {
        $this->geo_score = $geo_score;

        return $this;
    }

    public function getGeoSource(): ?string
    {
        return $this->geo_source;
    }

    public function setGeoSource(?string $geo_source): self
    {
        $this->geo_source = $geo_source;

        return $this;
    }

    public function getNumRouteOrId(): ?string
    {
        return $this->num_route_or_id;
    }

    public function setNumRouteOrId(string $num_route_or_id): self
    {
        $this->num_route_or_id = $num_route_or_id;

        return $this;
    }

    public function getNumRouteComId(): ?string
    {
        return $this->num_route_or_id;
    }

    public function setNumRouteComId(string $num_route_com_id): self
    {
        $this->num_route_com_id = $num_route_com_id;

        return $this;
    }

    public function getVoie(): ?int
    {
        return $this->voie;
    }

    public function setVoie(?int $voie): self
    {
        $this->voie = $voie;

        return $this;
    }

    public function getIndiceNumeriqueRoute(): ?string
    {
        return $this->indice_numerique_route;
    }

    public function setIndiceNumeriqueRoute(?string $indice_numerique_route): self
    {
        $this->indice_numerique_route = $indice_numerique_route;

        return $this;
    }

    public function getIndiceAlphabetiqueRoute(): ?string
    {
        return $this->indice_alphabetique_route;
    }

    public function setIndiceAlphabetiqueRoute(string $indice_alphabetique_route): self
    {
        $this->indice_alphabetique_route = $indice_alphabetique_route;

        return $this;
    }

    public function getNomRegion(): ?string
    {
        return $this->nom_region;
    }

    public function setNomRegion(?string $nom_region): self
    {
        $this->nom_region = $nom_region;

        return $this;
    }

    public function getGeoSourceDisplay(): ?string
    {
        return $this->geo_source_display;
    }

    public function setGeoSourceDisplay(?string $geo_source_display): self
    {
        $this->geo_source_display = $geo_source_display;

        return $this;
    }

    public function getPrDisplay(): ?float
    {
        return $this->pr_display;
    }

    public function setPrDisplay(float $pr_display): self
    {
        $this->pr_display = $pr_display;

        return $this;
    }

    /**
     * @return Collection|AccidentCaracteristique[]
     */
    public function getAccidentcaracteristique(): Collection
    {
        return $this->accidentcaracteristiques;
    }

    public function addAccidentcaracteristique(AccidentCaracteristique $accidentcaracteristique): self
    {

        if (!$this->accidentcaracteristiques->contains($accidentcaracteristique)) {
            $this->accidentcaracteristiques[] = $accidentcaracteristique;
            $accidentcaracteristique->setNumAccident($this->accidentcaracteristiques);
        }

        return $this;
    }

    public function removeAccidentcaracteristique(AccidentCaracteristique $accidentcaracteristiques): self
    {
        if ($this->accidentcaracteristiques->contains($accidentcaracteristiques)) {
            $this->accidentcaracteristiques->removeElement($accidentcaracteristiques);
        }

        return $this;
    }
}
