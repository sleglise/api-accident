<?php

namespace App\Entity;

use App\Repository\TopPlageHoraireRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TopPlageHoraireRepository::class)
 */
class TopPlageHoraire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $plage_horaire;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlageHoraire(): ?string
    {
        return $this->plage_horaire;
    }

    public function setPlageHoraire(string $plage_horaire): self
    {
        $this->plage_horaire = $plage_horaire;

        return $this;
    }

    public function getNb(): ?int
    {
        return $this->nb;
    }

    public function setNb(int $nb): self
    {
        $this->nb = $nb;

        return $this;
    }
}
