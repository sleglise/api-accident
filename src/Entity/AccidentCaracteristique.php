<?php

namespace App\Entity;

use App\Repository\AccidentCaracteristiqueRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass=AccidentCaracteristiqueRepository::class)
 * @ApiResource(
 *      normalizationContext={"groups"={"read:accident"}},
 *      collectionOperations={"get"},
 *      itemOperations={"get"}
 * )
 */
class AccidentCaracteristique
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="AccidentCorporel", inversedBy="accidentcaracteristiques")
     * @ORM\JoinColumn(name="num_accident", referencedColumnName="num_accident")
     */
    private $num_accident;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $luminosite;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $agglomeration;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $intersection;


    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $meteo;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $type_collision;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $distance;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $categorie_route;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $regime_circulation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_voies_circulation;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $numero_pr_rapprochement;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5, nullable=true)
     */
    private $distance_pr_rapprochement;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $voie_reservee;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $profil_route;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $plan_route;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $largeur_terre_plein_central;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $largeur_chaussee;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $etat_route;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $infrastructure_route;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $situation_accident;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $environnement_special;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_voiture;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $presence_voiture;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_moto;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $presence_moto;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_velo;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $presence_velo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_poids_lourd;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $presence_poid_lourd;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_autre_vehicule;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $presence_autre_vehicule;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_pieton;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $presence_pieton;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_indemne;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_blesse_leger;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_hospitalise;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_tue;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $gravite_accident;

    public function getNumAccident(): ?int
    {
        return $this->num_accident;
    }

    public function setNumAccident(?int $num_accident): self
    {
        $this->num_accident = $num_accident;

        return $this;
    }

    public function getLuminosite(): ?string
    {
        return $this->luminosite;
    }

    public function setLuminosite(?string $luminosite): self
    {
        $this->luminosite = $luminosite;

        return $this;
    }

    public function getAgglomeration(): ?string
    {
        return $this->agglomeration;
    }

    public function setAgglomeration(?string $agglomeration): self
    {
        $this->agglomeration = $agglomeration;

        return $this;
    }

    public function getIntersection(): ?string
    {
        return $this->intersection;
    }

    public function setIntersection(?string $intersection): self
    {
        $this->intersection = $intersection;

        return $this;
    }

    public function getMeteo(): ?string
    {
        return $this->meteo;
    }

    public function setMeteo(string $meteo): self
    {
        $this->meteo = $meteo;

        return $this;
    }

    public function getTypeCollision(): ?string
    {
        return $this->type_collision;
    }

    public function setTypeCollision(?string $type_collision): self
    {
        $this->type_collision = $type_collision;

        return $this;
    }


    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(?float $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getCategorieRoute(): ?string
    {
        return $this->categorie_route;
    }

    public function setCategorieRoute(?string $categorie_route): self
    {
        $this->categorie_route = $categorie_route;

        return $this;
    }


    public function getRegimeCirculation(): ?string
    {
        return $this->regime_circulation;
    }

    public function setRegimeCirculation(?string $regime_circulation): self
    {
        $this->regime_circulation = $regime_circulation;

        return $this;
    }

    public function getNombreVoiesCirculation(): ?int
    {
        return $this->nombre_voies_circulation;
    }

    public function setNombreVoiesCirculation(?int $nombre_voies_circulation): self
    {
        $this->nombre_voies_circulation = $nombre_voies_circulation;

        return $this;
    }

    public function getNumeroPrRapprochement(): ?string
    {
        return $this->numero_pr_rapprochement;
    }

    public function setNumeroPrRapprochement(string $numero_pr_rapprochement): self
    {
        $this->numero_pr_rapprochement = $numero_pr_rapprochement;

        return $this;
    }

    public function getDistancePrRapprochement(): ?string
    {
        return $this->distance_pr_rapprochement;
    }

    public function setDistancePrRapprochement(?string $distance_pr_rapprochement): self
    {
        $this->distance_pr_rapprochement = $distance_pr_rapprochement;

        return $this;
    }

    public function getVoieReservee(): ?string
    {
        return $this->voie_reservee;
    }

    public function setVoieReservee(?string $voie_reservee): self
    {
        $this->voie_reservee = $voie_reservee;

        return $this;
    }

    public function getProfilRoute(): ?string
    {
        return $this->profil_route;
    }

    public function setProfilRoute(?string $profil_route): self
    {
        $this->profil_route = $profil_route;

        return $this;
    }

    public function getPlanRoute(): ?string
    {
        return $this->plan_route;
    }

    public function setPlanRoute(string $plan_route): self
    {
        $this->plan_route = $plan_route;

        return $this;
    }

    public function getLargeurTerrePleinCentral(): ?float
    {
        return $this->largeur_terre_plein_central;
    }

    public function setLargeurTerrePleinCentral(?float $largeur_terre_plein_central): self
    {
        $this->largeur_terre_plein_central = $largeur_terre_plein_central;

        return $this;
    }

    public function getLargeurChaussee(): ?float
    {
        return $this->largeur_chaussee;
    }

    public function setLargeurChaussee(?float $largeur_chaussee): self
    {
        $this->largeur_chaussee = $largeur_chaussee;

        return $this;
    }

    public function getEtatRoute(): ?string
    {
        return $this->etat_route;
    }

    public function setEtatRoute(?string $etat_route): self
    {
        $this->etat_route = $etat_route;

        return $this;
    }

    public function getInfrastructureRoute(): ?string
    {
        return $this->infrastructure_route;
    }

    public function setInfrastructureRoute(?string $infrastructure_route): self
    {
        $this->infrastructure_route = $infrastructure_route;

        return $this;
    }

    public function getSituationAccident(): ?string
    {
        return $this->situation_accident;
    }

    public function setSituationAccident(?string $situation_accident): self
    {
        $this->situation_accident = $situation_accident;

        return $this;
    }

    public function getEnvironnementSpecial(): ?string
    {
        return $this->environnement_special;
    }

    public function setEnvironnementSpecial(?string $environnement_special): self
    {
        $this->environnement_special = $environnement_special;

        return $this;
    }

    public function getNombreVoiture(): ?int
    {
        return $this->nombre_voiture;
    }

    public function setNombreVoiture(?int $nombre_voiture): self
    {
        $this->nombre_voiture = $nombre_voiture;

        return $this;
    }

    public function getPresenceVoiture(): ?string
    {
        return $this->presence_voiture;
    }

    public function setPresenceVoiture(?string $presence_voiture): self
    {
        $this->presence_voiture = $presence_voiture;

        return $this;
    }

    public function getNombreMoto(): ?int
    {
        return $this->nombre_moto;
    }

    public function setNombreMoto(?int $nombre_moto): self
    {
        $this->nombre_moto = $nombre_moto;

        return $this;
    }

    public function getPresenceMoto(): ?string
    {
        return $this->presence_moto;
    }

    public function setPresenceMoto(string $presence_moto): self
    {
        $this->presence_moto = $presence_moto;

        return $this;
    }

    public function getNombreVelo(): ?int
    {
        return $this->nombre_velo;
    }

    public function setNombreVelo(?int $nombre_velo): self
    {
        $this->nombre_velo = $nombre_velo;

        return $this;
    }

    public function getPresenceVelo(): ?string
    {
        return $this->presence_velo;
    }

    public function setPresenceVelo(string $presence_velo): self
    {
        $this->presence_velo = $presence_velo;

        return $this;
    }

    public function getNombrePoidsLourd(): ?int
    {
        return $this->nombre_poids_lourd;
    }

    public function setNombrePoidsLourd(?int $nombre_poids_lourd): self
    {
        $this->nombre_poids_lourd = $nombre_poids_lourd;

        return $this;
    }

    public function getPresencePoidLourd(): ?string
    {
        return $this->presence_poid_lourd;
    }

    public function setPresencePoidLourd(?string $presence_poid_lourd): self
    {
        $this->presence_poid_lourd = $presence_poid_lourd;

        return $this;
    }

    public function getNombrePieton(): ?int
    {
        return $this->nombre_pieton;
    }

    public function setNombrePieton(?int $nombre_pieton): self
    {
        $this->nombre_pieton = $nombre_pieton;

        return $this;
    }

    public function getPresencePieton(): ?string
    {
        return $this->presence_pieton;
    }

    public function setPresencePieton(?string $presence_pieton): self
    {
        $this->presence_pieton = $presence_pieton;

        return $this;
    }

    public function getNombreAutreVehicule(): ?int
    {
        return $this->nombre_autre_vehicule;
    }

    public function setNombreAutreVehicule(?int $nombre_autre_vehicule): self
    {
        $this->nombre_autre_vehicule = $nombre_autre_vehicule;

        return $this;
    }

    public function getPresenceAutreVehicule(): ?string
    {
        return $this->presence_autre_vehicule;
    }

    public function setPresenceAutreVehicule(?string $presence_autre_vehicule): self
    {
        $this->presence_autre_vehicule = $presence_autre_vehicule;

        return $this;
    }

    public function getNombreIndemne(): ?int
    {
        return $this->nombre_indemne;
    }

    public function setNombreIndemne(?int $nombre_indemne): self
    {
        $this->nombre_indemne = $nombre_indemne;

        return $this;
    }

    public function getNombreBlesseLeger(): ?int
    {
        return $this->nombre_blesse_leger;
    }

    public function setNombreBlesseLeger(?int $nombre_blesse_leger): self
    {
        $this->nombre_blesse_leger = $nombre_blesse_leger;

        return $this;
    }

    public function getNombreHospitalise(): ?int
    {
        return $this->nombre_hospitalise;
    }

    public function setNombreHospitalise(?int $nombre_hospitalise): self
    {
        $this->nombre_hospitalise = $nombre_hospitalise;

        return $this;
    }

    public function getNombreTue(): ?int
    {
        return $this->nombre_tue;
    }

    public function setNombreTue(?int $nombre_tue): self
    {
        $this->nombre_tue = $nombre_tue;

        return $this;
    }

    public function getGraviteAccident(): ?string
    {
        return $this->gravite_accident;
    }

    public function setGraviteAccident(?string $gravite_accident): self
    {
        $this->gravite_accident = $gravite_accident;

        return $this;
    }
}
